package by.gsu.igi.lectures.lecture10;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class BoxLayoutDemo {

    public static void main(String[] args) {
        BoxLayoutDemo gui = new BoxLayoutDemo();
        gui.go();
    }

    private void go() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setBackground(Color.DARK_GRAY);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JButton button1 = new JButton("Кнопка");
        JButton button2 = new JButton("Еще одна кнопка");

        panel.add(button1);
        panel.add(button2);

        frame.getContentPane().add(BorderLayout.EAST, panel);
        frame.setSize(250, 200);
        frame.setVisible(true);
    }
}
