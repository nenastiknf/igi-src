package by.gsu.igi.lectures.lecture05;

/**
 * @author Evgeniy Myslovets
 */
public class ExceptionDemo {

    public static class FooException extends Exception {
        public FooException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static void main(String[] args) throws FooException {
        try {
            String str = null;
            System.out.println(str.length());
        } catch (NullPointerException e) {
            throw new FooException("Объект не проинициализирован", e);
        } catch (Exception e) {
            System.err.println("У нас баг в программе, пожалуйста отправьте сообщение об ошибке");
            e.printStackTrace();
        } finally {
            System.out.println("Пока!");
        }
        System.out.println("Пока еще разок!");
    }
}

